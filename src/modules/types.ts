import { ModuleContext } from "@graphql-modules/core";

export type Message = {
    id: number,
    text: string
}

export type Messages = {
    userId: number
    messages: Message[]
}

export type QueryArgs<T = {}> = {
    root: any,
    args: T,
    context: ModuleContext
    info: any
}

export type User = {
    id: number,
    username: string
}

export type UserArgs = {
    id: number
}
