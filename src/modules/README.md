# Modules
Self-contained bits that can be utilized on the Apollo server and can utilize each other as well.

## Definition File
The `graphql.d.ts` allows Typescript to understand the `.graphql` files that contain the schemas in a friendly, readable
way.

## Types File
Just a place for common and shared types.

## Providers File
This file is used for the common bits of providers, an interface and a `symbol` that will be used to associate an
instance of a provider. This will allow for GraphQL Module's
[dependency injection](https://graphql-modules.com/docs/introduction/dependency-injection) to wire together the
providers.

A module like the `UsersModule` will setup it's provider like:
```typescript
const options: GraphQLModuleOptions<any, any, any, any> = {
    typeDefs,
    resolvers,
    providers: [{
        provide: USER_PROVIDER, // Symbol or string
        useClass: UserModuleProvider // Class to instantiate
    }]
};
export const UsersModule = new GraphQLModule(options);
```

The provider of the `HelloModule` relies on the providers of the `UsersModule` and `MessagesModule` to get user and
message data. It uses GraphQL Module's
[dependency injection](https://graphql-modules.com/docs/introduction/dependency-injection) to wire in the needed
providers.
```typescript
@Injectable()
export class HelloModuleProvider implements HelloProvider{
    private msgProvider: MessageProvider;
    private userProvider: UserProvider;

    constructor(@Inject(MESSAGE_PROVIDER) msgProvider: MessageProvider,
                @Inject(USER_PROVIDER) userProvider: UserProvider) {
        this.msgProvider = msgProvider;
        this.userProvider = userProvider;
    }
    // ...
}
```
