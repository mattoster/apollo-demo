import { Inject, Injectable } from "@graphql-modules/di";
import { User } from "../types";
import { HelloProvider, MESSAGE_PROVIDER, MessageProvider, USER_PROVIDER, UserProvider } from "../providers";

@Injectable()
export class HelloModuleProvider implements HelloProvider {
    private msgProvider: MessageProvider;
    private userProvider: UserProvider;

    constructor(@Inject(MESSAGE_PROVIDER) msgProvider: MessageProvider,
                @Inject(USER_PROVIDER) userProvider: UserProvider) {
        this.msgProvider = msgProvider;
        this.userProvider = userProvider;
    }

    getGreetingByUserId(id: number): string {
        const user: User | undefined = this.userProvider.getUserById(id);
        if (user) {
            return `Hello, ${user.username}!`;
        }
        else {
            return `Hello, unknown user!`;
        }
    }

    getFullGreetingByUserId(id: number): string {
        const count: number = this.msgProvider.getMessageCountByUserId(id);
        const user: User | undefined = this.userProvider.getUserById(id);
        if (user) {
            return `Hello, ${user.username}, you have ${count} messages!`;
        }
        else {
            return `Hello, unknown user!`;
        }
    }

}
