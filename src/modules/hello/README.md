# Hello Module
A simple module that collects two simple endpoints, schema and resolvers, into a `GraphQLModule` that is used by the
Apollo server.

## Endpoints
The obligatory `greeting` that returns the obligatory `Hello, World!`. Also `greetingWithName` which accepts a name and
returns a customized greeting.

There are additional endpoints that utilize providers from the `UsersModule` and `MessagesModule`.

## Wiring it Up
Because the `HelloModule` depends on the other modules, it is configured like:
```typescript
const options: GraphQLModuleOptions<any, any, any, any> = {
    // ...
    imports: [ UsersModule, MessagesModule ],
    // ...
};
export const HelloModule: GraphQLModule = new GraphQLModule(options);
```
This ensures that the modules `HelloModule` depends on are loaded before this module is. It also allows for the
providers in those modules to be created and ready for injection before this module is loaded.
