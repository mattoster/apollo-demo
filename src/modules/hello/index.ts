import { GraphQLModule, GraphQLModuleOptions } from "@graphql-modules/core";

import * as typeDefs from "./schema.graphql";
import resolvers from "./resolvers";
import { UsersModule } from "../users";
import { MessagesModule } from "../messages";
import { HELLO_PROVIDER } from "../providers";
import { HelloModuleProvider } from "./provider";

const options: GraphQLModuleOptions<any, any, any, any> = {
    typeDefs,
    resolvers,
    imports: [UsersModule, MessagesModule],
    providers: [{
        provide: HELLO_PROVIDER,
        useClass: HelloModuleProvider
    }]
};
export const HelloModule: GraphQLModule = new GraphQLModule(options);
