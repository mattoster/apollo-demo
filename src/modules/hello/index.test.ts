import "reflect-metadata"
import { HelloModule } from "./index";
import { HELLO_PROVIDER, HelloProvider } from "../providers";
import gql from "graphql-tag";
import { executeQuery } from "../../__tests__/utils";

const fullMsg: string = "Hello testy.mctestface, you have 3 messages!";
const partialMsg: string = "Hello testy.mctestface!";
const unknownMsg: string = "Hello unknown user!";

class MockHelloProvider implements HelloProvider {
    getFullGreetingByUserId(id: number): string {
        return id === 0 ? fullMsg : unknownMsg;
    }

    getGreetingByUserId(id: number): string {
        return id === 0 ? partialMsg : unknownMsg;
    }
}

const testProvider = {
    provide: HELLO_PROVIDER,
    overwrite: true,
    useClass: MockHelloProvider
};

describe("Hello module unit tests", () => {
    const { injector, schema } = HelloModule;

    beforeAll(() => {
        injector.provide(testProvider);
    });

    describe("greetingById queries", () => {

        it("test greetingById resolver returns a partial greeting from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingById(id: 0)
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["greetingById"]).toBe(partialMsg);
        });

        it("test greetingById resolver returns an unknown greeting from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingById(id: 1)
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["greetingById"]).toBe(unknownMsg);
        });

        it("test greetingById resolver returns errors with incorrect input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingById(id: "1")
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

        it("test greetingById resolver returns errors with no input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingById
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

    });

    describe("greetingFullById queries", () => {

        it("test greetingFullById resolver returns a full greeting from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingFullById(id: 0)
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["greetingFullById"]).toBe(fullMsg);
        });

        it("test greetingFullById resolver returns an unknown greeting from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingFullById(id: 1)
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["greetingFullById"]).toBe(unknownMsg);
        });

        it("test greetingFullById resolver returns errors with incorrect input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingFullById(id: "1")
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

        it("test greetingFullById resolver returns errors with no input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    greetingFullById
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

    });

});
