import { ModuleContext } from "@graphql-modules/core";
import { HELLO_PROVIDER, HelloProvider } from "../providers";
import { UserArgs } from "../types";

type GreetingArgs = {
    name: string
}

export default {
    Query: {
        greeting: (): string => "Hello, World!",
        greetingWithName: (obj: any, args: GreetingArgs): string => {
            return `Hello, ${args.name}!`;
        },
        greetingById: (obj: any, args: UserArgs, context: ModuleContext): string => {
            return context.injector.get<HelloProvider>(HELLO_PROVIDER).getGreetingByUserId(args.id);
        },
        greetingFullById: (obj: any, args: UserArgs, context: ModuleContext) => {
            return context.injector.get<HelloProvider>(HELLO_PROVIDER).getFullGreetingByUserId(args.id);
        }
    }
};
