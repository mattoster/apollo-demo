import { ModuleContext } from "@graphql-modules/core";
import { User, UserArgs } from "../types";
import { USER_PROVIDER, UserProvider } from "../providers";

export default {
    Query: {
        users: (obj: any, args: {}, context: ModuleContext) => {
            return context.injector.get<UserProvider>(USER_PROVIDER).getUsers();
        },
        user: (obj: any, args: UserArgs, context: ModuleContext) => {
            return context.injector.get<UserProvider>(USER_PROVIDER).getUserById(args.id);
        }
    },
    User: {
        id: (user: User) => user.id,
        username: (user: User) => user.username
    }
};
