import { GraphQLModule, GraphQLModuleOptions } from "@graphql-modules/core";
import * as typeDefs from "./schema.graphql";
import resolvers from "./resolvers";
import { USER_PROVIDER } from "../providers";
import { UserModuleProvider } from "./provider";

const options: GraphQLModuleOptions<any, any, any, any> = {
    typeDefs,
    resolvers,
    providers: [{
        provide: USER_PROVIDER,
        useClass: UserModuleProvider
    }]
};
export const UsersModule = new GraphQLModule(options);
