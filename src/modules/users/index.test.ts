import "reflect-metadata"
import { UsersModule } from "./index";
import { USER_PROVIDER, UserProvider } from "../providers";
import gql from "graphql-tag";
import { User } from "../types";
import { executeQuery } from "../../__tests__/utils";

const testUser: User = {
    id: 0,
    username: "testy.mctestface"
};

class MockUserProvider implements UserProvider {

    getUserById(id: number): User | undefined {
        return id === 0 ? testUser : undefined;
    }

    getUsers(): User[] {
        return [ testUser ];
    }
}

const testProvider = {
    provide: USER_PROVIDER,
    overwrite: true,
    useClass: MockUserProvider
};

describe("User module unit tests", () => {
    const { injector, schema } = UsersModule;

    beforeAll(() => {
        injector.provide(testProvider);
    });

    describe("user queries", () => {

        it("test user resolver returns a user from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    user(id: 0) {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["user"]["id"]).toBe(testUser.id);
            expect(result.data!["user"]["username"]).toBe(testUser.username);
        });

        it("test user resolver returns null user with incorrect id", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    user(id: 1) {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            // According to the schema, a null user can be returned in a query
            expect(result.data!["user"]).toBeNull();
        });

        it("test user resolver has errors with incorrect input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    user(id: "1") {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

        it("test user resolver has errors with no input", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    user {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

    });

    describe("users queries", () => {

        it("test users resolver returns a user array from query", async () => {
            const result = await executeQuery(schema, gql`
                query {
                    users {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeFalsy();
            expect(result.data).toBeDefined();
            expect(result.data!["users"][0]["id"]).toBe(testUser.id);
            expect(result.data!["users"][0]["username"]).toBe(testUser.username);
        });

        it.skip("test users resolver returns errors with input params", async () => {
            // This query should definitely cause errors, but it doesn't. WAT?
            const result = await executeQuery(schema, gql`
                query {
                    users(id: 1) {
                        id,
                        username
                    }
                }
            `);

            expect(result).toBeDefined();
            expect(result.errors).toBeTruthy();
        });

    });

});
