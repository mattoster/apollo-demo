import { Injectable } from "@graphql-modules/di";
import { User } from "../types";
import { UserProvider } from "../providers";

@Injectable()
export class UserModuleProvider implements UserProvider {
    // TODO: compose persistent or API client stuff here instead of hard coded nonsense
    users: User[] = [{
        id: 0,
        username: "john.doe"
    }, {
        id: 2,
        username: "maggie.smith"
    }, {
        id: 4,
        username: "jake.the.snake"
    }, {
        id: 6,
        username: "mary.mary.quite.contrary"
    }];

    getUserById(id: number): User | undefined {
        return this.users.find((user: User) => {
            return user.id === id;
        });
    }

    getUsers(): User[] {
        return this.users;
    }

}
