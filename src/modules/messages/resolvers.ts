import { ModuleContext } from "@graphql-modules/core";
import { Message, UserArgs } from "../types";
import { MESSAGE_PROVIDER, MessageProvider } from "../providers";

export default {
    Query: {
        messagesByUserId: (obj: any, args: UserArgs, context: ModuleContext) => {
            return context.injector.get<MessageProvider>(MESSAGE_PROVIDER).getMessagesByUserId(args.id);
        },
        messageCountByUserId: (obj: any, args: UserArgs, context: ModuleContext) => {
            return context.injector.get<MessageProvider>(MESSAGE_PROVIDER).getMessageCountByUserId(args.id);
        }
    },
    Message: {
        id: (msg: Message) => msg.id,
        text: (msg: Message) => msg.text
    }
};
