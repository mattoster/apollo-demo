import { Injectable } from "@graphql-modules/di";
import { Message, Messages, User } from "../types";
import { MessageProvider } from "../providers";

@Injectable()
export class MessageModuleProvider implements MessageProvider {
    // TODO: compose persistent or API client stuff here instead of hard coded nonsense
    messages: Messages[] = [{
        userId: 0,
        messages: [{
            id: 123,
            text: "Hello, how are you?"
        }, {
            id: 234,
            text: "I am doing well, and yourself?"
        }]
    }, {
        userId: 2,
        messages: [{
            id: 345,
            text: "you can shove this job"
        }, {
            id: 456,
            text: "Well, I never!"
        }, {
            id: 901,
            text: "yeah, i'll bet you have!"
        }]
    }, {
        userId: 4,
        messages: [{
            id: 567,
            text: "Where are you? I said circle the bank and pick us up!"
        }, {
            id: 678,
            text: "I heard sirens so i bailed...:("
        }]
    }, {
        userId: 6,
        messages: [{
            id: 789,
            text: "Y U no call n e moar? lolz"
        }, {
            id: 890,
            text: "new phone who dis?"
        }]
    }];

    getMessagesByUserId(id: number): Message[] | undefined {
        const msgs: Messages | undefined = this.messages.find((msgs: Messages) => {
            return msgs.userId === id;
        });
        return msgs ? msgs.messages : undefined;
    }

    getMessageCountByUserId(id: number): number {
        const msgs: Messages | undefined = this.messages.find((msgs: Messages) => {
            return msgs.userId === id;
        });
        return msgs ? msgs.messages.length : 0;
    }

}
