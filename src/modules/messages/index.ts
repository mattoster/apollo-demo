import { GraphQLModule, GraphQLModuleOptions } from "@graphql-modules/core";
import * as typeDefs from "./schema.graphql";
import resolvers from "./resolvers";
import { MessageModuleProvider } from "./provider";
import { MESSAGE_PROVIDER } from "../providers";

const options: GraphQLModuleOptions<any, any, any, any> = {
    typeDefs,
    resolvers,
    providers: [{
        provide: MESSAGE_PROVIDER,
        useClass: MessageModuleProvider
    }]
};
export const MessagesModule = new GraphQLModule(options);
