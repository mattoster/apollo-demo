import { Message, User } from "./types";

export const HELLO_PROVIDER = Symbol("HelloProvider");

export interface HelloProvider {
    getGreetingByUserId(id: number): string;

    getFullGreetingByUserId(id: number): string
}


export const MESSAGE_PROVIDER = Symbol("MessageProvider");

export interface MessageProvider {
    getMessagesByUserId(id: number): Message[] | undefined;

    getMessageCountByUserId(id: number): number;
}


export const USER_PROVIDER = Symbol("UserProvider");

export interface UserProvider {
    getUserById(id: number): User | undefined;

    getUsers(): User[];
}
