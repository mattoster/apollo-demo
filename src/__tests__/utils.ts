import { DocumentNode, execute, GraphQLSchema } from "graphql";

export const executeQuery = async (schema: GraphQLSchema, document: DocumentNode) => {
    const execution = {
        schema,
        document
    };
    return await execute(execution);
};
