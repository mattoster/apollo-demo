import "reflect-metadata"
import { ApolloServer } from "apollo-server";
import { environment } from "./environment";
import { ApolloServerExpressConfig } from "apollo-server-express";
import { HelloModule } from "./modules/hello";


const apolloServerConfig: ApolloServerExpressConfig = {
    modules: [ HelloModule ],
    context: session => session,
    introspection: environment.apollo.introspection,
    playground: environment.apollo.playground
};
const server: ApolloServer = new ApolloServer(apolloServerConfig);


server.listen(environment.port).then(({ url }) => console.log(`Server ready at ${url}.`));
console.log("Hello, World!");


// Hot Module Replacement
if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => console.log("Module disposed!"));
}
