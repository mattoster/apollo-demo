# Apollo Server Demo
Setup an [Apollo GraphQL](https://www.apollographql.com/docs/) server using
[GraphQL modules](https://graphql-modules.com/docs/introduction/getting-started).

## ENV
The Apollo server has a development UI built-in that can be used to build and call queries. The UI and other dev
friendly items should be configured as off, but enabled when the environment is dev.

The `.env` file specifies how Apollo should be configured and `dotenv` is used in a script to ensure Apollo is started
up using the configuration in `.env`.

## Modules
Create a simple set of modules that can be combined and used by the Apollo server. Add in a `UsersModule`, and
`MessagesModule` which will be used by the `HelloModule` to aggregate data from the other two modules for use by some of
its queries.

## GraphQL Query
An example query:
```graphql
{
  greeting,
  greetingWithName(name: "steve"),
  greetingById(id: 2),
  greetingFullById(id: 2),
  users {
    id,
    username
  },
  user(id: 4) {
    username
  },
  messageCountByUserId(id: 2),
  messagesByUserId(id: 6){
    id,
    text
  }
}
```
An example response:
```json
{
  "data": {
    "greeting": "Hello, World!",
    "greetingWithName": "Hello, steve!",
    "greetingById": "Hello, maggie.smith!",
    "greetingFullById": "Hello, maggie.smith, you have 3 messages!",
    "users": [
      {
        "id": 0,
        "username": "john.doe"
      },
      {
        "id": 2,
        "username": "maggie.smith"
      },
      {
        "id": 4,
        "username": "jake.the.snake"
      },
      {
        "id": 6,
        "username": "mary.mary.quite.contrary"
      }
    ],
    "user": {
      "username": "jake.the.snake"
    },
    "messageCountByUserId": 3,
    "messagesByUserId": [
      {
        "id": 789,
        "text": "Y U no call n e moar? lolz"
      },
      {
        "id": 890,
        "text": "new phone who dis?"
      }
    ]
  }
}
```

## Getting Started
Pull down the source and install:
```bash
npm install
```

Build:
```bash
npm run build
```

Start in dev mode:
```bash
start:env
```

You should now be able to open [Apollo's Playground](http://localhost:4001/) to begin playing around with the schema!
